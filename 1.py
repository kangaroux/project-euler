""" 1.py

Problem: https://projecteuler.net/problem=1
Runtime: ~30ms

Prints the sum of all numbers below 1000 which are multiples of 3 or 5
"""

def multiples():
	""" Generator that returns numbers divisible by 3 or 5 """
	i = 3

	while True:
		if i % 3 == 0 or i % 5 == 0:
			yield i

		i += 1

if __name__ == "__main__":
	sum = 0

	for i in multiples():
		if i >= 1000:
			break

		sum += i

	print("Sum of multiples of 3 or 5 below 1000: %i" % sum)
