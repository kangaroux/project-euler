""" 2.py

Problem: https://projecteuler.net/problem=2
Runtime: ~30ms

Prints the sum of all even valued terms in the Fibonacci sequence
that do not exceed 4,000,000.
"""

def fibonacci():
	""" Generator that returns terms of the fibonacci sequence """
	a = 1
	b = 2

	# Terms 1 and 2
	yield a
	yield b

	while True:
		# Swap and add to get the next term
		a, b = b, (a + b)

		yield b


if __name__ == "__main__":
	sum = 0

	for i in fibonacci():
		if i > 4000000:
			break
		elif i % 2 == 0:
			sum += i

	print("Sum of even fibonacci terms below 4 million: %i" % sum)
