""" 3.py

Problem: https://projecteuler.net/problem=3
Runtime: ~90ms

Prints the largest prime factor of the number 600851475143
"""

import math


def factors(n):
	""" Generator that returns all factors of a number except 1 and itself.
	This generates factors in descending order, so the largest factors are
	yielded first
	"""
	factors = []
	square = math.floor(math.sqrt(n))

	for i in range(2, square):
		factor = n / i

		if not factor.is_integer():
			continue

		factor = int(factor)
		factors.append(factor)

		yield factor

	# Factors come in pairs which we can calculate now that we know the
	# factors which are greater than the sqrt(n)
	for f in factors[::-1]:
		# Don't repeat the square root factor
		if f == square:
			continue

		yield n // f

def is_prime(n):
	""" Returns True if the number is prime. This uses the 6x +/- 1 algorithm
	found here: https://en.wikipedia.org/wiki/Primality_test#Simple_methods
	"""
	if n <= 1:
		return False
	elif n <= 3:
		return True
	elif n % 2 == 0 or n % 3 == 0:
		return False

	i = 5

	while (i * i) < n:
		if n % i == 0 or n % (i + 2) == 0:
			return False

		i += 6

	return True

if __name__ == "__main__":
	for i in factors(600851475143):
		if is_prime(i):
			print("The largest prime factor of 600851475143 is: %i" % i)
			break
