""" 4.py

Problem: https://projecteuler.net/problem=4
Runtime: ~40ms

Prints the largest palindrome made from the product of two 3 digit numbers
"""

def is_palindrome(n):
	""" Returns True if the given number is a palindrome """
	s = str(n)

	return s == s[::-1]

def products(n):
	""" Generator which returns all unique products of two n-digit numbers

	Because we're concerned with the largest product, a simple nested loop
	isn't ideal because one of the numbers is constantly going down to 1.

	e.g.: 99 * 99, 99 * 98, ..., 99 x 1

	Instead of setting our lower limit to 1, we'll reduce it 10 at a time.

	(99..90) * (99..90)    100   products
	(99..80) * (89..80)    1000  products
	(99..70) * (79..70)    10000 products

	In reality these product numbers are even lower because we can skip
	(1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9) = 45 products due to duplicates
	"""

	# Upper and lower limits
	upper = 10**n - 1
	lower = upper - 9

	a = upper
	b = upper

	while True:
		for i in range(upper, max(0, lower - 1), -1):
			# Each iteration wastes `iteration` number of calculations.
			# These are calculations that have already been done in previous
			# iterations but with the terms flipped
			iteration = upper - i

			for j in range(lower + (9 - iteration), max(0, lower - 1), -1):
				yield (i, j)

		if lower == 0:
			break

		lower -= 10

for p in products(3):
	product = p[0] * p[1]

	if is_palindrome(product):
		print("The largest palindrome of two 3-digit numbers (%d * %d) is: %d"
			% (p[0], p[1], product))
		break
