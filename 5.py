""" 5.py

Problem: https://projecteuler.net/problem=5
Runtime: ~40s

Prints the smallest number that is evenly divisible by all numbers 1..20
"""

def is_divisible(n, factors):
	""" Returns True if `n` evenly divides into each factor in `factors` """
	for f in factors:
		if n % f != 0:
			return False

	return True

if __name__ == "__main__":
	n = 2520

	# Because `n` is larger than any of the factors we're interested in,
	# we can filter out some of the terms. For example, if a number is
	# evenly divisible by 2 and 3, then it must be divisible by 6. Any term
	# that isn't a prime and can be expressed as a product of two smaller
	# terms gets filtered
	factors = (2, 3, 4, 5, 7, 9, 11, 13, 16, 17, 19, 20)

	while not is_divisible(n, factors):
		n += 2

	print("The smallest number evenly divisible by all numbers 1 through 20 is: %d" % n)
