""" 6.py

Problem: https://projecteuler.net/problem=6
Runtime: ~30ms

Prints the difference between the sum of the squares and square of the sum
of the numbers 1..100
"""

MAX = 100

if __name__ == "__main__":
	a = sum([ x for x in range(1, MAX + 1) ]) ** 2
	b = sum([ x**2 for x in range(1, MAX + 1) ])

	print("The difference between [...] is: %d" % (a - b))
